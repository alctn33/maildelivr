You must install go and postgresql before using project. 
Also enter your database credentials in fields database/db_handler.go
Then, you can run service with go run main.go 

api list: 

GET : /contacts

retrieves contacts for a given user

Example request body:
{
"userid": 1,
"contacts": []
}

Example reponse body:
{"userid":1,"contacts":[{"name":"ali","mail":"alctn33@gmail.com","id":1},{"name":"veli","mail":"asdjk @askdj.com","id":2}]}


POST: /contacts

Adds contacts to given users contact list

Example request body:

{
"userid": 1,
"contacts": [{
"name": "ali",
"mail": "alctn33@gmail.com",
"id": 1
},
{
"name": "veli",
"mail": "asdjk @askdj.com",
"id": 1
}
]
}

Example reponse body:
{"userid":1,"contacts":[{"name":"ali","mail":"alctn33@gmail.com","id":1},{"name":"veli","mail":"asdjk @askdj.com","id":2}]}

GET: /mail

Gets mail list of an user

Example Request Body: 
{"senderID" : 1}
Example Response Body
[{"mailID":1,"message":"asdkjkajsd"},{"mailID":2,"message":"asdkjkfgggfgfajsd"}]

POST: /mail

sends email to given recipient list for given user

Example Request Body

{
"senderID" : 1,
"message" : "asdkjkfgggfgfajsd",
"receivers" : [1,2]
}

GET: /receivedetails

Gets details of given email id

Example Request Body: {
"emailId": 0
}

Example Response Body: [{"id":2,"sentTime":"2021-11-20T20:43:25.627366Z","clickTime":{"Time":"0001-01-01T00:00:00Z","Valid":false},"isClicked":false},{"id":1,"sentTime":"2021-11-20T20:43:24.324013Z","clickTime":{"Time":"2021-11-20T21:28:18.66482Z","Valid":true},"isClicked":true},{"id":4,"sentTime":"2021-11-22T12:21:24.072377Z","clickTime":{"Time":"0001-01-01T00:00:00Z","Valid":false},"isClicked":false},{"id":3,"sentTime":"2021-11-22T12:21:22.902063Z","clickTime":{"Time":"2021-11-22T12:23:04.250688Z","Valid":true},"isClicked":true}]

psql commands for database: 

-- Database: contactDB

-- DROP DATABASE IF EXISTS "contactDB";

CREATE DATABASE "contactDB"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Turkish_Turkey.1254'
    LC_CTYPE = 'Turkish_Turkey.1254'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE "contactDB"
    IS 'picus task';

-- Table: public.receiverdetails

-- DROP TABLE IF EXISTS public.receiverdetails;

CREATE TABLE IF NOT EXISTS public.receiverdetails
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    link character varying(6) COLLATE pg_catalog."default" NOT NULL,
    email_id integer NOT NULL,
    sent_time timestamp without time zone,
    click_time timestamp without time zone,
    isclicked boolean,
    CONSTRAINT receiverdetails_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.receiverdetails
    OWNER to postgres;
	
-- Table: public.emails

-- DROP TABLE IF EXISTS public.emails;

CREATE TABLE IF NOT EXISTS public.emails
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    owner integer NOT NULL,
    message text COLLATE pg_catalog."default",
    CONSTRAINT emails_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.emails
    OWNER to postgres;
	
-- Table: public.contacts

-- DROP TABLE IF EXISTS public.contacts;

CREATE TABLE IF NOT EXISTS public.contacts
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    owner_id integer NOT NULL,
    name text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT contacts_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.contacts
    OWNER to postgres;