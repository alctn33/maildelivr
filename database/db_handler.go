package database

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"maildelivr/data"
	"time"
)

const (
	DB_USER     = "postgres"
	DB_PASSWORD = "3647549"
	DB_NAME     = "contactDB"
)

var DB *sql.DB

// DB set up
func SetupDB() *sql.DB {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)

	if err != nil {
		panic(err)
	}

	return db
}
func CheckDBConnection() {
	if DB == nil {
		DB = SetupDB()
	}
}
func CheckErr(err error) {
	if err != nil {
		log.Println(err.Error())
		panic(err)
	}
}

func GetContacts(ownerID int) []data.Contact {
	CheckDBConnection()
	rows, err := DB.Query("SELECT * FROM contacts where owner_id = $1", ownerID)
	CheckErr(err)
	var contacts []data.Contact
	for rows.Next() {
		var id int
		var ownerId int
		var name string
		var email string

		err = rows.Scan(&id, &ownerId, &name, &email)

		// check errors
		if err != nil {
			log.Println(err.Error())
			panic(err)
		}
		contacts = append(contacts, data.Contact{Name: name, Mail: email, ID: id})
	}
	return contacts
}

func AddContact(ownerID int, contact data.Contact) int {
	CheckDBConnection()
	result, err := DB.Exec("INSERT INTO contacts (owner_id, name, email) VALUES ($1, $2, $3)", ownerID, contact.Name, contact.Mail)
	CheckErr(err)
	lastID, err := result.LastInsertId()
	return int(lastID)
}

func GetReceiveDetails(emailID int) []data.ReceiveDetailsResponse {
	CheckDBConnection()
	rows, err := DB.Query("SELECT * FROM receiverdetails where email_id = $1", emailID)
	CheckErr(err)
	var receiveInfos []data.ReceiveDetailsResponse
	for rows.Next() {
		var id int
		var l string
		var emailId int
		var sentTime time.Time
		var clickTime sql.NullTime
		var isClicked bool
		err = rows.Scan(&id, &l, &emailId, &sentTime, &clickTime, &isClicked)
		receiveInfos = append(receiveInfos, data.ReceiveDetailsResponse{id, sentTime, clickTime, isClicked})
	}
	return receiveInfos
}

func CheckLinkExist(link string) bool {
	CheckDBConnection()
	rows, err := DB.Query("SELECT * FROM receiverdetails WHERE link = $1", link)
	CheckErr(err)
	if rows.Next() {
		log.Println("found")
		return true
	} else {
		log.Println("not found")
		return false
	}
}

func GetEmail(contactID int) string {
	CheckDBConnection()
	row, err := DB.Query("SELECT email FROM contacts where id = $1", contactID)
	CheckErr(err)
	var email string
	if row.Next() {
		err = row.Scan(&email)
		CheckErr(err)
	}
	return email
}

func AddEmail(owner int, message string) int {
	CheckDBConnection()
	result, err := DB.Exec("INSERT INTO emails (owner, message) VALUES($1, $2)", owner, message)
	CheckErr(err)
	lastID, err := result.LastInsertId()
	return int(lastID)
}

func AddReceiverDetail(link string, emailID int) int {
	CheckDBConnection()
	result, err := DB.Exec("INSERT INTO receiverdetails (link, email_id, sent_time, isClicked) VALUES ($1, $2, $3, false )", link, emailID, time.Now())
	CheckErr(err)
	lastID, err := result.LastInsertId()
	return int(lastID)
}

func GetEmails(owner int) []data.ResponseMails {
	CheckDBConnection()
	rows, err := DB.Query("SELECT * FROM emails where owner = $1", owner)
	CheckErr(err)
	var emails []data.ResponseMails
	for rows.Next() {
		var id int
		var owner int
		var message string
		err = rows.Scan(&id, &owner, &message)
		CheckErr(err)
		emails = append(emails, data.ResponseMails{id, message})
	}
	return emails
}

func GetReceiveDetailsForLink(link string) *data.ReceiveDetails {

	row, err := DB.Query("SELECT * FROM receiverdetails where link = $1", link)
	if err != nil {
		panic(err)
	} else {
		if row.Next() {
			var id int
			var l string
			var emailId int
			var sentTime time.Time
			var clickTime sql.NullTime
			var isClicked bool
			err = row.Scan(&id, &l, &emailId, &sentTime, &clickTime, &isClicked)
			CheckErr(err)
			return &data.ReceiveDetails{id, l, emailId, sentTime, clickTime, isClicked}
		}
	}
	return nil
}

func AddClickReceiveDetails(id int) {
	_, err := DB.Exec("UPDATE receiverdetails SET isclicked = true, click_time = $1 WHERE id = $2", time.Now(), id)
	CheckErr(err)
}
