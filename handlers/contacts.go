package handlers

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"maildelivr/data"
	"maildelivr/database"
	"net/http"
)

type Contacts struct {
	l *log.Logger
}

func (c Contacts) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if request.URL.Path != "/contacts" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}

	if request.Method == "GET" {
		GetContacts(writer, request)
	}

	if request.Method == "POST" {
		AddContacts(writer, request)
	}
}

func getContactList(requestBody io.ReadCloser) data.ContactList {
	body, err := ioutil.ReadAll(requestBody)
	CheckErr(err)
	var t data.ContactList
	err = json.Unmarshal(body, &t)
	CheckErr(err)
	return t
}

func GetContacts(writer http.ResponseWriter, request *http.Request) {
	contactList := getContactList(request.Body)
	var response = data.ContactList{Userid: contactList.Userid, Contacts: database.GetContacts(contactList.Userid)}
	err := json.NewEncoder(writer).Encode(response)
	CheckErr(err)
}

func AddContacts(writer http.ResponseWriter, request *http.Request) {
	contactList := getContactList(request.Body)
	contacts := database.GetContacts(contactList.Userid)
	hMap := make(map[string]string)
	for _, contact := range contacts {
		hMap[contact.Mail] = contact.Name
	}

	for _, contact := range contactList.Contacts {
		_, present := hMap[contact.Mail]
		if present {
			// name update can wait for now, todo
			/*if name != contact.Name {
				_, err := db.Query("UPDATE contacts SET name = $1 where owner_id = $2 and email = $3", contact.Name, t.Userid, contact.Mail)
				CheckErr(err)
			}*/
		} else {
			database.AddContact(contactList.Userid, contact)
		}
	}
	var response = data.ContactList{Userid: contactList.Userid, Contacts: database.GetContacts(contactList.Userid)}
	err := json.NewEncoder(writer).Encode(response)
	CheckErr(err)
}
func NewContacts(l *log.Logger) *Contacts {
	return &Contacts{l}
}
