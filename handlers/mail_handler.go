package handlers

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"maildelivr/data"
	"maildelivr/database"
	"maildelivr/mail"
	"math/rand"
	"net/http"
	"time"
)

type MailHandler struct {
}

func NewMailHandler() *MailHandler {
	return &MailHandler{}
}
func (m MailHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if request.URL.Path != "/mail" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}
	if request.Method == "GET" {
		GetMailDetails(writer, request)
	}
	if request.Method == "POST" {
		SendEmail(writer, request)
	}
}
func getMailRequest(requestBody io.ReadCloser) data.Mail {
	body, err := ioutil.ReadAll(requestBody)
	CheckErr(err)
	var mailRequest data.Mail
	err = json.Unmarshal(body, &mailRequest)
	CheckErr(err)
	return mailRequest
}
func GetMailDetails(writer http.ResponseWriter, request *http.Request) {
	mailRequest := getMailRequest(request.Body)
	response := database.GetEmails(mailRequest.SenderID)
	err := json.NewEncoder(writer).Encode(response)
	CheckErr(err)
}
func RandomSeq() string {
	//https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go/22892986#22892986
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, 6)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
func CreateLink() string {
	for {
		createdLink := RandomSeq()
		log.Println(createdLink)
		if !database.CheckLinkExist(createdLink) {
			return createdLink
		}
	}
}
func SendEmail(writer http.ResponseWriter, request *http.Request) {
	mailRequest := getMailRequest(request.Body)
	receivers := GetEmailsFromIDs(mailRequest.ReceiverIDs)

	mailSender := mail.NewMailSender()
	mail.Authenticate(mailSender)

	lastInsertID := database.AddEmail(mailRequest.SenderID, mailRequest.Message)
	for _, receiver := range receivers {
		link := CreateLink()
		message := mailRequest.Message + "\n Please click this link: http://localhost:9090/received/" + link
		mail.SendMail(mailSender, receiver, message)
		database.AddReceiverDetail(link, lastInsertID)
	}
}
func GetEmailsFromIDs(ReceiverIDs []int) []string {
	var emails []string
	for _, receiverID := range ReceiverIDs {
		email := database.GetEmail(receiverID)
		emails = append(emails, email)
	}
	return emails
}
