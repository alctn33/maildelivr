package handlers

import (
	"log"
	"maildelivr/database"
	"net/http"
)

type Receive struct {
	l *log.Logger
}

func NewReceivedHandler(l *log.Logger) *Receive {
	return &Receive{l}
}

func (r Receive) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	log.Println(request.URL.Path)
	if request.Method != "GET" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
	}
	link := request.URL.Path[len(request.URL.Path)-6:]
	log.Println(link)
	receiveDetails := database.GetReceiveDetailsForLink(link)
	if receiveDetails != nil {
		if !receiveDetails.IsClicked {
			database.AddClickReceiveDetails(receiveDetails.Id)
		} else {
			log.Println("link already clicked")
		}
	}
}
