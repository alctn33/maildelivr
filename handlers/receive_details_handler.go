package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"maildelivr/data"
	"maildelivr/database"
	"net/http"
)

type ReceiveDetailsHandler struct {
	l *log.Logger
}

func NewReceiveDetailsHandler(l *log.Logger) *ReceiveDetailsHandler {
	return &ReceiveDetailsHandler{l}
}

func (r ReceiveDetailsHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if request.URL.Path != "/receivedetails" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}

	if request.Method != "GET" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}
	body, err := ioutil.ReadAll(request.Body)
	CheckErr(err)
	var t data.ReceiveDetailsRequest
	err = json.Unmarshal(body, &t)
	CheckErr(err)
	response := database.GetReceiveDetails(t.EmailId)
	err = json.NewEncoder(writer).Encode(response)
	CheckErr(err)
}
