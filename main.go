package main

import (
	"log"
	"maildelivr/handlers"
	"net/http"
	"os"
)

func main() {
	//return
	l := log.New(os.Stdout, "product-api", log.LstdFlags)

	//sm := http.NewServeMux()
	ch := handlers.NewContacts(l)
	mh := handlers.NewMailHandler()
	rh := handlers.NewReceivedHandler(l)
	rdh := handlers.NewReceiveDetailsHandler(l)
	//sm.Handle("/contacts", ch)

	http.HandleFunc("/contacts", func(w http.ResponseWriter, r *http.Request) {
		ch.ServeHTTP(w, r)
	})
	http.HandleFunc("/mail", func(w http.ResponseWriter, r *http.Request) {
		mh.ServeHTTP(w, r)
	})
	http.HandleFunc("/received/", func(w http.ResponseWriter, r *http.Request) {
		rh.ServeHTTP(w, r)
	})
	http.HandleFunc("/receivedetails", func(w http.ResponseWriter, r *http.Request) {
		rdh.ServeHTTP(w, r)
	})
	if err := http.ListenAndServe(":9090", nil); err != nil {
		log.Fatal(err)
	}
}
