package mail

import (
	"fmt"
	"net/smtp"
)

type MailSender struct {
	from     string
	password string
	smtpHost string
	smtpPort string
	auth     smtp.Auth
}

func NewMailSender() *MailSender {
	mailSender := MailSender{"picustask@gmail.com",
		"Pcs2647129+",
		"smtp.gmail.com",
		"587",
		nil}
	return &mailSender
}

func Authenticate(sender *MailSender) {
	auth := smtp.PlainAuth("", sender.from, sender.password, sender.smtpHost)
	sender.auth = auth
}
func SendMail(sender *MailSender, receiver string, message string) {
	rcv := []string{receiver}
	err := smtp.SendMail(sender.smtpHost+":"+sender.smtpPort,
		sender.auth,
		sender.from,
		rcv,
		[]byte(message))
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Email Sent Successfully!")
}
