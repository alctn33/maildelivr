package data

import (
	"database/sql"
	"time"
)

type Contact struct {
	Name string `json:"name"`
	Mail string `json:"mail"`
	ID   int    `json:"id"`
}
type ContactList struct {
	Userid   int       `json:"userid"`
	Contacts []Contact `json:"contacts"`
}

type ReceiveDetailsRequest struct {
	EmailId int `json:"emailId"`
}
type ReceiveDetailsResponse struct {
	Id        int          `json:"id"`
	SentTime  time.Time    `json:"sentTime"`
	ClickTime sql.NullTime `json:"clickTime"`
	IsClicked bool         `json:"isClicked"`
}
type ReceiveDetails struct {
	Id        int          `json:"id"`
	Link      string       `json:"link"`
	EmailID   int          `json:"emailID"`
	SentTime  time.Time    `json:"sentTime"`
	ClickTime sql.NullTime `json:"clickTime"`
	IsClicked bool         `json:"isClicked"`
}

type Mail struct {
	SenderID    int    `json:"senderID"`
	Message     string `json:"message"`
	ReceiverIDs []int  `json:"receivers"`
}
type ResponseMails struct {
	MailID  int    `json:"mailID"`
	Message string `json:"message"`
}
